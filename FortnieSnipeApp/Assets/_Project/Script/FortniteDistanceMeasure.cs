using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//#JAMSCENTER#
/// <summary>
/// Script write by Eloi Stree (DefaultCompany). 
/// Check the following link to acknowledge the license.
/// License: "https://www.patreon.com/jamscenter"
/// Created: 3/31/2018 4:06:25 PM 
/// </summary>
[HelpURL("https://www.patreon.com/jamscenter")]
public class FortniteDistanceMeasure : MonoBehaviour
{

    public float _distanceInMeter = 100;
    public RectTransform _startPosition;
    public RectTransform _endPosition;

    [Header("Debug (Don't touch)")]
    [SerializeField]
#pragma warning disable CS0414  
    private float _unityDistance;

   

    public static FortniteDistanceMeasure InstanceInScene { get { return _instanceInScene; } }
    private static FortniteDistanceMeasure _instanceInScene;
    private static FortniteDistanceMeasure _i;
    public void Awake()
    {
        _instanceInScene = this;
        _i = this;
    }

    public static float GetDistanceBetween(RectTransform start, RectTransform end)
    {
        if (start == null || end == null)
            throw new NotMyResponsabilityException("Please fill correclty the parameters");

        float result = 0;
        float refDistance = _i.GetReferencialDistance();
        float unitymeasureOnDistance = _i.GetUnityDistance();
        float askedUnityMeasure = Vector3.Distance(Position(start), Position(end));

        result = (refDistance / unitymeasureOnDistance) * askedUnityMeasure;

        return result;
    }
    public float GetUnityUnitByMeter()
    {
        return GetUnityDistance() / GetReferencialDistance();

    }
    public float GetMetersByUnityDistance()
    {
        return  GetReferencialDistance()/ GetUnityDistance();
    }


    private float GetReferencialDistance()
    {
        return _distanceInMeter;
    }
    private float GetUnityDistance()
    {
        if (ClassFieldNotValide()) return 0;
        return Vector3.Distance(Position(_startPosition), Position(_endPosition));
    }

    private bool ClassFieldNotValide()
    {

        return _startPosition == null || _endPosition == null;
    }

    private void OnValidate()
    {
        _unityDistance = GetUnityDistance();
        Debug.DrawLine(Position(_startPosition), Position(_endPosition), Color.green, 60);
    }

    public static Vector3 Position(RectTransform transform)
    {
        return transform.position;
    }
}
