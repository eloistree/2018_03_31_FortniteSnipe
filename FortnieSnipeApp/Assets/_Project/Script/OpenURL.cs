﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script write by Eloi Strée. 
/// Check the following link to aknowldege the license.
/// License: "https://www.patreon.com/jamscenter"
/// </summary>
[HelpURL("https://www.patreon.com/jamscenter")]
public class OpenURL : MonoBehaviour {
    public string _urlToOpen ="https://www.google.com";
    public void OpenTheLinkedUrl() {
        Application.OpenURL(_urlToOpen);
    }
}
