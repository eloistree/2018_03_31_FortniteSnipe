using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


/// <summary>
/// Script write by Eloi Stree (DefaultCompany). 
/// Check the following link to acknowledge the license.
/// License: "https://www.patreon.com/jamscenter"
/// Created: 3/31/2018 6:32:52 PM 
/// </summary>
[HelpURL("https://www.patreon.com/jamscenter")]
public class MapInteractionBehaviour : MonoBehaviour {

    public GameObject _zoomLogic;
    public GameObject _moveLogic;
    public GameObject _drawLogic;

    public enum Interaction { Zoom, Move, Draw}

    public void SetInteractionTo(string interaction)
    {
        if (interaction == "Zoom") { SetInteractionTo(Interaction.Zoom); }
        else
        if (interaction == "Move") { SetInteractionTo(Interaction.Move); }
        else
        if (interaction == "Draw") { SetInteractionTo(Interaction.Draw); }

    }
        public void SetInteractionTo(Interaction interaction) {
        DisableAll();

        switch (interaction)
        {
            case Interaction.Zoom:
                _zoomLogic.SetActive(true);
                break;
            case Interaction.Move:
                _moveLogic.SetActive(true);
                break;
            case Interaction.Draw:
                _drawLogic.SetActive(true);
                break;
            default:
                break;
        }
    }

    private void DisableAll()
    {
        _zoomLogic.SetActive(false);
        _moveLogic.SetActive(false);
        _drawLogic.SetActive(false);
    }

    public void Awake()
    {
        DisableAll();
    }
}
