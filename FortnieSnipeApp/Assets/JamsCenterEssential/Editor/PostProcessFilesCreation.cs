﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Script write by Eloi Stree (#AUTHOR#). 
/// Check the following link to acknowledge the license.
/// License: "https://www.patreon.com/jamscenter"
/// Created: #TIME# 
/// </summary>
[HelpURL("https://www.patreon.com/jamscenter")]
public class PostProcessFilesCreation : UnityEditor.AssetModificationProcessor
{

    public static void OnWillCreateAsset(string path)
    {

        path = path.Replace(".meta", "");
        int index = path.LastIndexOf(".");
        if (index < 0)
            return;

        string file = path.Substring(index);
        if (file != ".cs" && file != ".js" && file != ".boo")
            return;

        index = Application.dataPath.LastIndexOf("Assets");
        path = Application.dataPath.Substring(0, index) + path;
        if (!System.IO.File.Exists(path))
            return;
        string fileContent = System.IO.File.ReadAllText(path);
        if (fileContent.IndexOf("//#JAMSCENTER#") > -1) {

            fileContent = fileContent.Replace("//#JAMSCENTER#","");
            fileContent = fileContent.Replace("#AUTHOR#", Application.companyName);
            fileContent = fileContent.Replace("#TIME#", DateTime.Now.ToString());
        }
         
        System.IO.File.WriteAllText(path, fileContent);
        AssetDatabase.Refresh();
    }
    
}
